# List of Oral Exam Exercises

Please choose one of the following solved exercises from your textbook, and
schedule a time to meet with me to present the problem, and solution to me.  I
will be asking additional questions to test your understanding.  So, be prepared
to discuss related questions.

* Chapter 1, Solved Exercise 2
* Chapter 3, Solved Exercise 2
* Chapter 4, Solved Exercise 1
* Chapter 4, Solved Exercise 3
* Chapter 5, Solved Exercise 2
* Chapter 6, Solved Exercise 1
* Chapter 7, Solved Exercise 2
